var regexEmail =
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var errors = "";

var grandTotal = 0;
const fixedTax = 0.13

function isValidNumber(event) {
  var selectedCharacter = String.fromCharCode(event.which);
  if (!/[0-9]/.test(selectedCharacter)) {
    event.preventDefault();
  }
}

function formSubmit(e) {
  e.preventDefault();
  var name = document.getElementById("customerName").value.trim();
  var email = document.getElementById("email").value.trim();

  errors = "";
  document.getElementById("errors").innerHTML = errors;

  validateInputRegex(email, regexEmail, "Error, Please Enter a valid Email");

  if (errors) {
    document.getElementById("errors").innerHTML = errors;
  } else {
    getCartItems();
    receipt(name, email);
    return false;
  }

  return false;
}

function getCartItems() {
  var tshirts = document.getElementById("tshirts").value;
  var caps = document.getElementById("caps").value;
  var jackets = document.getElementById("jackets").value;

  if (isNaN(tshirts) || isNaN(caps) || isNaN(jackets)) {
    errors = "Please Enter a Valid Quantity";
    document.getElementById("errors").innerHTML = errors;
  } else {
    if (tshirts > 0 || caps > 0 || jackets > 0) {
      document.getElementById("errors").innerHTML = "";
      addToTable("T-Shirt", tshirts, 15); 
      addToTable("Caps", caps, 50); 
      addToTable("Jackets", jackets, 10);
    } else {
      errors = "Add some items please";
      document.getElementById("errors").innerHTML = errors;
    }

    return false;
  }
}

function addToTable(itemName, quantity, itemPrice) {
  if (quantity > 0) {
    var itemFound = false;
    var table = document.getElementById("receipt");
    var totalRows = table.getElementsByTagName("tr");

    for (i = 1; i < totalRows.length - 1; i++) {
      var firstCell = totalRows[i].getElementsByTagName("td")[0];
      if (firstCell.innerText == itemName) {
        itemFound = true;
        var newQuantity = quantity;
        var calculatedPrice = Number.parseInt(newQuantity * itemPrice);
        totalRows[i].getElementsByTagName("td")[1].innerHTML = newQuantity;
        totalRows[i].getElementsByTagName("td")[2].innerHTML = itemPrice;
        totalRows[i].getElementsByTagName("td")[3].innerHTML = calculatedPrice;
      }
    }

    //  adding a new selectedRow
    if (!itemFound) {
      var selectedRow = table.insertRow(totalRows.length - 3);
      var newItemName = selectedRow.insertCell(0);
      var newQuantity = selectedRow.insertCell(1);
      var unitPrice = selectedRow.insertCell(2);
      var newPrice = selectedRow.insertCell(3);
      newItemName.innerHTML = itemName;
      newQuantity.innerHTML = quantity;
      unitPrice.innerHTML = itemPrice;
      newPrice.innerHTML = quantity * itemPrice;
    }
  }
}

function receipt(name, email) {
  document.getElementsByClassName("formData")[0].style.display = 'block'
  document.getElementById("nameValue").innerHTML = name;
  document.getElementById("emailValue").innerHTML = email;
  document.getElementById("phoneNumberReceipt").innerHTML =
    document.getElementById("cellnumber").value;

  var table = document.getElementById("receipt");
  var totalRows = table.getElementsByTagName("tr");

  if (totalRows.length) {
    var totalPrice = 0;
    for (i = 1; i < totalRows.length - 3; i++) {
      var priceCell = totalRows[i].getElementsByTagName("td")[3];
      var priceCellValue = parseInt(priceCell.innerText);
      totalPrice += priceCellValue;
    }
  }

  var calculatedTax = fixedTax * totalPrice;

  grandTotal = totalPrice + calculatedTax;
  document.getElementById("nonTaxAmount").innerHTML = totalPrice;
  document.getElementById("taxAmount").innerHTML = calculatedTax.toFixed(2);
  document.getElementById("grandTotal").innerHTML = grandTotal;
}

function validateInputRegex(inputType, regexPattern, errorMessage) {
  if (!regexPattern.test(inputType)) {
    errors += `${errorMessage}<br>`;
  }
}
// this function formsats phone number
function formatCellNumber() {
  var phnumber = document.getElementById("cellnumber");
  var x = phnumber.value.replace(/\D/g, "").match(/(\d{3})(\d{3})(\d{4})/);
  phnumber.value = x[1] + "-" + x[2] + "-" + x[3];
}